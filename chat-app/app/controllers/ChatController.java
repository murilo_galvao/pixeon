package controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import model.User;
import play.mvc.Controller;
import play.mvc.*;

@Singleton
public class ChatController extends Controller{

	private static Map<String,WebSocket.Out> outMap = new HashMap<>();
	private static Map<String,User> users = new HashMap<>();
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public LegacyWebSocket<String> feedChat(String idUser, String name, String img) {

		User user = new User();
		user.setId(idUser);
		user.setName(name);
		user.setImg(img);

        return WebSocket.whenReady((in, out) -> {
            outMap.forEach((id,out1)->{
            	out1.write(user.getName()+" is online");
            });

            users.put(user.getId(),user);
            outMap.put(user.getId(),out);

            // For each event received on the socket,
            in.onMessage((message) -> {
                outMap.forEach((id,out1)->{
                    String write = user.getName() + " say: " + message;
                    out1.write(write);
                });
            });
            

            // When the socket is closed.
            in.onClose(() ->{
                users.remove(user.getId());
                outMap.remove(user.getId());
                outMap.forEach((id,out1)->{
                	out1.write(user.getName() + " is logout");
                });
            });
        });
    }
}
