(function(){
   'use strict';

   angular
   .module('chat-app')
   .factory('ChatService', ChatService);

   ChatService.$inject = ['$websocket'];

   function ChatService($websocket){
      var dataStream;
      var collection = [];

      var methods = {
        collection: collection,
        get: function(message) {
           dataStream.send(message);
        }
      };

      var _init = function(user){
         dataStream = $websocket('ws://localhost:9000/chatWS?idUser=' + user.id + '&name=' + user.name + '&img=' + user.img);
         dataStream.onMessage(function(message) {
            var msg = message.data;
            collection.push(msg);
         });
      };
      return {
        methods: methods,
        init: _init
      };
   }
})();
