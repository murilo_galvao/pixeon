(function () {
   'use strict';

   angular.module('chat-app', ['ngRoute', 'ngWebSocket','ui.router', 'angular-google-gapi','ngCookies']);
})();