(function(){
   'use strict';

   angular
   .module('chat-app')
   .run(googleRun);

   googleRun.$inject = ['GAuth', 'GApi', 'GData', '$rootScope', '$window', '$cookies', '$state'];

   function googleRun(GAuth, GApi, GData, $rootScope, $window, $cookies, $state){
      $rootScope.gdata = GData;

      var CLIENT = '696149797195-b2kml58b4l7kdi6287cungtj7hr5ifqk.apps.googleusercontent.com';

      GApi.load('calendar', 'v3');
      GAuth.setClient(CLIENT);
      GAuth.setScope('https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/calendar.readonly');

      var currentUser = $cookies.get('userId');
      if(currentUser){
         GData.setUserId(currentUser);
         GAuth.checkAuth().then(
            function(){
               if($state.includes('login'))
                  $state.go('chat');
            },
            function(){
               $state.go('login');
            }
         );
      } else {
         $state.go('login');
      }

      $rootScope.logout = function() {
         GAuth.logout().then(function () {
             $cookies.remove('userId');
             $cookies.remove('userName');
             $cookies.remove('pictureName');
             $state.go('login');
         });
      };
   }
})();