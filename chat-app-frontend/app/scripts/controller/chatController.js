(function () {
   'use strict';

   angular.module('chat-app').controller('ChatController', ChatController);

   ChatController.$inject = ['$scope', 'ChatService', '$cookies'];

   function ChatController($scope, ChatService, $cookies){

      $scope.send = function(){
         ChatService.methods.get($scope.message);
         delete $scope.message;
      }

      $scope.init = function(){
         var user = {
            id: $cookies.get('userId'),
            name: $cookies.get('userName'),
            img: $cookies.get('pictureName')
         };
         ChatService.init(user);
         $scope.MyData = ChatService.methods;
      }

      $scope.init();
   };
})();
