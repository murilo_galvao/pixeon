(function () {
   'use strict';

   angular.module('chat-app').controller('LoginController', LoginController);

   LoginController.$inject = ['$scope', 'GAuth', '$cookies','GData', '$state'];

   function LoginController($scope, GAuth, $cookies, GData, $state){
      $scope.gdata = GData;
      
      console.log("GDATA",$scope.gdata);

      var ifLogin = function(){
         $cookies.put('userId', GData.getUserId());
         $cookies.put('userName', $scope.gdata.getUser().name);
         $cookies.put('pictureName', $scope.gdata.getUser().picture);
         $state.go('chat');
      };

      $scope.doLogin = function(){
         GAuth.checkAuth().then(
            function(){
               ifLogin();
            },
            function(){
               GAuth.login().then(function(){
                  
               console.log('2');
                  ifLogin();
               });
            }
         );
      }
   };
})();
