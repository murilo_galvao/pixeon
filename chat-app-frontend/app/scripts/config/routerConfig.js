(function () {
   'use strict';

   angular
   .module('chat-app')
   .config(routerConfig);

   routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

   function routerConfig($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise("/");

      $stateProvider
      .state('login', {
         url: "/",
         templateUrl: "fonts/login/login.html",
         controller: "LoginController"
      })
      .state('chat', {
         url: "/chat",
         templateUrl: "fonts/chat/chat.html",
         controller: "ChatController"
      })

   }
})();
